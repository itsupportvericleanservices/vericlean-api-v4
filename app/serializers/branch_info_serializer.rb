class BranchInfoSerializer < ActiveModel::Serializer
  attributes :id, :name, :site_code, :address, :address2, :state, :city, :zipcode, :max_time, :duration_min, :duration_max
  belongs_to :client, serializer: ClientInfoSerializer
end
