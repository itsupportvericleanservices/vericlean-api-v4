class ReassignedNearmeSerializer < ActiveModel::Serializer
  attributes :id, :nearme, :service_status, :service_type, :service_started, :service_finished, :working_seconds
  belongs_to :branch, serializer: BranchInfoSerializer

  def nearme
    nearme_ids = @instance_options[:nearme_ids]
    nearme_ids.include?(object.branch_id)
  end

  def service_type
    if object.reassigned == true
      service_type = "reassigned"
    end

    if object.rescheduled == true
      service_type = "rescheduled"
    end
    service_type
  end

  def service_status
    service_status = "pending"
    if !object.started_at.nil?
      service_status = "in_progress"
    end

    if !object.finished_at.nil?
      service_status = "complete"
    end
    service_status
  end

  def service_started
    service_started = ""
    if !object.started_at.nil?
      service_started = object.started_at
    end
    service_started
  end

  def service_finished
    service_finished = ""
    if !object.finished_at.nil?
      service_finished = object.finished_at
    end
    service_finished
  end

  def working_seconds
    working_seconds = 0
    if !object.started_at.nil?
      working_seconds = (DateTime.now.in_time_zone - object.started_at).to_i
    end
    working_seconds
  end

end
