class ManagersBranchesSerializer < ActiveModel::Serializer
  attributes :id, :name, :site_code, :address, :address2, :state, :city, :zipcode, :max_time, :duration_min, :duration_max, :route_history_id, :service_status, :nearme
  belongs_to :client, serializer: ClientInfoSerializer

  def nearme
    nearme_ids = @instance_options[:nearme_ids]
    nearme_ids.include?(object.id)
  end

  def service_status
    service_status = "pending"
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        service_status = "in_progress"
        if rh.started_at.nil?
          service_status = "pending"
        end

        if !rh.finished_at.nil?
          service_status = "complete"
        end
      end
    end
    service_status
  end

  def route_history_id
    route_history_id = ""
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        if !rh.started_at.nil? && rh.finished_at.nil?
          route_history_id = rh.id
        end
      end
    end
    route_history_id
  end

end
