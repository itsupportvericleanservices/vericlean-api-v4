class SupervisorActiveNearmeSerializer < SitesActiveNearmeSerializer
  attributes :id, :name, :site_code, :address, :address2, :state, :city, :zipcode, :max_time, :duration_min, :duration_max, :route_history_id, :service_status, :service_started, :service_finished, :working_seconds, :nearme, :cleaner_route_history
  belongs_to :client, serializer: ClientInfoSerializer

  def route_history_id
    route_history_id = ""
    route_histories = @instance_options[:route_histories]
    current_user = @instance_options[:current_user]
    route_histories.each do |rh|
      if rh.branch_id == (object.id) && rh.assigned_id == current_user.id
        route_history_id = rh.id
      end
    end
    route_history_id
  end

  def cleaner_route_history
    @instance_options[:route_histories]
    cleaner_route_history = ""
    route_histories = @instance_options[:route_histories]
    current_user = @instance_options[:current_user]
    route_histories.each do |rh|
      if rh.assigned_id == rh.cleaner_id && rh.branch_id == (object.id)
        cleaner_route_history = rh
      end
    end
    cleaner_route_history
  end
end