class ServicesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_service, only: [:show, :update, :destroy]

  def index
    @services = Service.all

    render json: @services, include: [:branch, :service_types] #verificar progreso
  end

  def show
    render json: @service, include: [:branch, :service_types] #verificar progreso
  end

  def create
    @service = Service.new(services_params)
    params['service_types'].each do |service_types|
      @service.service_types << ServiceType.find(service_types)
    end

    if @service.save
      render json: @service, status: :created
    else
      render json: @service.errors, status: :unprocessable_entity
    end
  end

  def update
    #Check out
    #@service_progress = ServiceProgress.new(services_progress_params)

    if @service.update(services_params) # && @service_progress.save

      params['service_types'].each do |service_types|
        @service.service_types << ServiceType.find(service_types)
      end

      render json: @service #verificar progreso
    else
      render json: @service.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @service.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_service
      @service = Service.find(params[:id])
    end

    def services_params
      params.permit(
        :branch_id,
        :route_schedule_id,
        :status, 
        :started_at, 
        :finished_at,
        service_types: [:id])
    end

    def services_progress_params
      params.permit(
        :service_id,
        :service_type_id)
    end
end
