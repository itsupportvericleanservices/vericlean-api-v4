class SupervisorsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_supervisor, only: [:show, :update, :destroy]
  
  def index
    @supervisors = Supervisor.all
  
    render json: @supervisors, include: [:role]
  end
  
  def show
    render json: @supervisor, include: [:role]
  end

  def create
    @supervisor = Supervisor.new(supervisor_params)

    if @supervisor.save
      render json: @supervisor, status: :created, location: @supervisor
    else
      render json: @supervisor.errors, status: :unprocessable_entity
    end
  end

  def update
    if @supervisor.update(supervisor_params)
      render json: @supervisor
    else
      render json: @supervisor.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @supervisor.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_supervisor
      @supervisor = Supervisor.find(params[:id])
    end

    def supervisor_params
      params.permit(
        :username,
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :role_id,
        :client_id)
    end
end
  