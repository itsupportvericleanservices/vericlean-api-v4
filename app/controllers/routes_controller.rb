class RoutesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_route, only: [:show, :update, :destroy]

  def index
    @routes = Route.all

    render json: @routes, include: [:branches, :user]
  end

  def show
    render json: @route, include: [:branches, :user]
  end

  def create
    @route = Route.new(routes_params)

    if @route.save
      render json: @route, status: :created
    else
      render json: @route.errors, status: :unprocessable_entity
    end
  end

  def update
    if @route.update(routes_params)
      render json: @route
    else
      render json: @route.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @route.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_route
      @route = Route.find(params[:id])
    end

    def routes_params
      params.permit(
          :user_id,
          :name)
    end
end
