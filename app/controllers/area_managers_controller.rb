class AreaManagersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_area_manager, only: [:show, :update, :destroy]
  
  def index
    @area_managers = AreaManager.all 
  
    render json: @area_managers, include: [:role]
  end
  
  def show
    render json: @area_manager, include: [:role]
  end

  def create
    @area_manager = AreaManager.new(area_manager_params)

    if @area_manager.save
      render json: @area_manager, status: :created, location: @area_manager
    else
      render json: @area_manager.errors, status: :unprocessable_entity
    end
  end

  def update
    if @area_manager.update(area_manager_params)
      render json: @area_manager
    else
      render json: @area_manager.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @area_manager.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_area_manager
      @area_manager = AreaManager.find(params[:id])
    end

    def area_manager_params
      params.permit(
        :username,
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :role_id,
        :client_id)
    end
end
  