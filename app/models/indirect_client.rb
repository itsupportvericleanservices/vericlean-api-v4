class IndirectClient < Client
  default_scope { where(indirect: 1) }
  belongs_to :client, foreign_key: 'indirect_id'
end
