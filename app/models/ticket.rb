class Ticket < ApplicationRecord
  	has_many :comments, as: :commentable
 	belongs_to :user
 	belongs_to :branch
 	has_many :work_orders
	# has_many_attached :images
	# has_many_attached :documents
	# has_many_attached :signatures
	# has_many_attached :photos
	# has_many_attached :invoices
end
