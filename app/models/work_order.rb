class WorkOrder < ApplicationRecord
  	# has_many :comments, as: :commentable
 	belongs_to :user
 	belongs_to :branch
 	belongs_to :client
	belongs_to :cleaner, :class_name => 'CleanerWorkorder', optional: :true
	belongs_to :ticket, optional: :true
	has_many :emails
	has_many_attached :images
	has_many_attached :documents
	has_many_attached :signatures
	has_many_attached :photos
	has_many_attached :invoices
end
