class AreaManager < User
  default_scope { where(role_id: 4) }

  has_many :branches
end