class UploadPhotoJob < ApplicationJob
  queue_as :default
  
  def perform(*args)
    tmp_file = Tempfile.new(args[1])
    tmp_file.binmode
    tmp_file.write(Base64.decode64(args[3].match(/base64,(.*)/)[1]))
    tmp_file.rewind()
    args[0].photos.attach(io: tmp_file, filename: args[1], content_type: args[2])
  end
end