# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_26_172916) do

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "branches", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "client_id"
    t.string "frequency"
    t.string "weekly_frequency"
    t.string "name"
    t.string "address"
    t.string "address2"
    t.string "state"
    t.string "city"
    t.string "zipcode"
    t.string "phone"
    t.string "email"
    t.string "contact_name"
    t.string "latitude"
    t.string "longitude"
    t.string "update_location_lat"
    t.string "update_location_lng"
    t.integer "user_request_update"
    t.integer "status_request_update", default: 0
    t.string "region"
    t.time "max_time"
    t.integer "duration_min"
    t.integer "duration_max"
    t.string "site_type"
    t.string "site_code"
    t.integer "cleanable_area"
    t.string "status"
    t.bigint "cleaner_id"
    t.bigint "area_manager_id"
    t.bigint "supervisor_id"
    t.integer "active", limit: 1, default: 1
    t.boolean "muted"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "deleted", default: false
    t.index ["area_manager_id"], name: "index_branches_on_area_manager_id"
    t.index ["cleaner_id"], name: "index_branches_on_cleaner_id"
    t.index ["client_id"], name: "index_branches_on_client_id"
    t.index ["supervisor_id"], name: "index_branches_on_supervisor_id"
  end

  create_table "branches_extra_area_managers", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "branch_id", null: false
    t.bigint "user_id", null: false
    t.index ["branch_id", "user_id"], name: "index_branches_extra_area_managers_on_branch_id_and_user_id"
    t.index ["user_id", "branch_id"], name: "index_branches_extra_area_managers_on_user_id_and_branch_id"
  end

  create_table "branches_extra_cleaners", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "branch_id", null: false
    t.bigint "user_id", null: false
    t.index ["branch_id", "user_id"], name: "index_branches_extra_cleaners_on_branch_id_and_user_id"
    t.index ["user_id", "branch_id"], name: "index_branches_extra_cleaners_on_user_id_and_branch_id"
  end

  create_table "branches_extra_supervisors", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "branch_id", null: false
    t.bigint "user_id", null: false
    t.index ["branch_id", "user_id"], name: "index_branches_extra_supervisors_on_branch_id_and_user_id"
    t.index ["user_id", "branch_id"], name: "index_branches_extra_supervisors_on_user_id_and_branch_id"
  end

  create_table "branches_service_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "branch_id"
    t.bigint "service_type_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_branches_service_types_on_branch_id"
    t.index ["service_type_id"], name: "index_branches_service_types_on_service_type_id"
  end

  create_table "clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "indirect", limit: 1, default: 0
    t.bigint "indirect_id"
    t.string "name"
    t.string "address"
    t.string "address2"
    t.string "state"
    t.string "city"
    t.string "zipcode"
    t.string "phone"
    t.string "email"
    t.string "contact_name"
    t.boolean "status"
    t.integer "active", limit: 1, default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "deleted", default: false
  end

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "commentable_id"
    t.string "commentable_type"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "emails", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "work_order_id"
    t.string "to"
    t.string "subject"
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_emails_on_user_id"
    t.index ["work_order_id"], name: "index_emails_on_work_order_id"
  end

  create_table "frequencies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mouses_tables", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.integer "age"
    t.datetime "birthdate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "user_id"
    t.boolean "success"
    t.string "message"
    t.datetime "sent_at"
    t.string "device_token"
    t.text "full_response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "photos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_id"], name: "index_photos_on_service_id"
    t.index ["user_id"], name: "index_photos_on_user_id"
  end

  create_table "roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "route_histories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "branch_id"
    t.bigint "user_id"
    t.bigint "assigned_id"
    t.bigint "cleaner_id"
    t.bigint "supervisor_id"
    t.bigint "area_manager_id"
    t.date "scheduled_date"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "latitude_checkin"
    t.string "longitude_checkin"
    t.string "latitude_checkout"
    t.string "longitude_checkout"
    t.boolean "supervisor"
    t.boolean "reassigned"
    t.boolean "rescheduled"
    t.integer "status"
    t.boolean "delayed", default: false
    t.index ["area_manager_id"], name: "index_route_histories_on_area_manager_id"
    t.index ["assigned_id"], name: "index_route_histories_on_assigned_id"
    t.index ["branch_id"], name: "index_route_histories_on_branch_id"
    t.index ["cleaner_id"], name: "index_route_histories_on_cleaner_id"
    t.index ["supervisor_id"], name: "index_route_histories_on_supervisor_id"
    t.index ["user_id"], name: "index_route_histories_on_user_id"
  end

  create_table "service_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.integer "min_time"
    t.integer "max_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_types_services", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "service_id"
    t.bigint "service_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["service_id"], name: "index_service_types_services_on_service_id"
    t.index ["service_type_id"], name: "index_service_types_services_on_service_type_id"
  end

  create_table "services", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "branch_id"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_services_on_branch_id"
  end

  create_table "text_messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "sent_to"
    t.string "from"
    t.string "to"
    t.text "body"
    t.text "twilio_body"
    t.string "message_type"
    t.string "hashtag"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_text_messages_on_user_id"
  end

  create_table "tickets", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.bigint "branch_id"
    t.bigint "user_id"
    t.string "request_contact"
    t.string "request_contact_email"
    t.string "request_contact_phone"
    t.datetime "event_date"
    t.datetime "due_date"
    t.string "short_description"
    t.string "description"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_tickets_on_branch_id"
    t.index ["user_id"], name: "index_tickets_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "role_id", default: 8
    t.string "device_token"
    t.bigint "client_id", default: 0
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "username"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.text "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "phone"
    t.string "address"
    t.boolean "status"
    t.string "area_manager"
    t.string "supervisor"
    t.string "secundary_cleaner"
    t.boolean "deleted", default: false
    t.index ["client_id"], name: "index_users_on_client_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "work_orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "internal_id"
    t.string "extra_id"
    t.bigint "user_id"
    t.bigint "branch_id"
    t.bigint "cleaner_id"
    t.bigint "client_id"
    t.bigint "ticket_id"
    t.datetime "requested_date"
    t.datetime "due_date"
    t.string "site_contact"
    t.string "site_phone"
    t.string "request_contact"
    t.string "request_phone"
    t.text "task_description"
    t.text "instructions"
    t.integer "status"
    t.integer "status_alt"
    t.string "main_type"
    t.integer "priority"
    t.decimal "fee", precision: 10
    t.datetime "scheduled_date"
    t.datetime "started_date"
    t.datetime "finished_date"
    t.string "started_lat"
    t.string "started_lng"
    t.string "finished_lat"
    t.string "finished_lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "task_name"
    t.text "signature"
    t.string "client_type"
    t.text "comments"
    t.string "request_contact_email"
    t.index ["branch_id"], name: "index_work_orders_on_branch_id"
    t.index ["cleaner_id"], name: "index_work_orders_on_cleaner_id"
    t.index ["client_id"], name: "index_work_orders_on_client_id"
    t.index ["ticket_id"], name: "index_work_orders_on_ticket_id"
    t.index ["user_id"], name: "index_work_orders_on_user_id"
  end

  add_foreign_key "branches", "clients"
  add_foreign_key "notifications", "users"
end
