class AddEmailToWo < ActiveRecord::Migration[5.2]
  def change
  	add_column :work_orders, :site_contact_email, :string
  end
end
