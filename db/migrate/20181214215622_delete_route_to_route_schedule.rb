class DeleteRouteToRouteSchedule < ActiveRecord::Migration[5.2]
  def change 
    change_table :route_schedules do |t|
      t.remove :route_id
    end  
  end
end
