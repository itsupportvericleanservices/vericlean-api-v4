class ChangeNotificationStatus < ActiveRecord::Migration[5.2]
  def change
    rename_column :notifications, :status, :success
  end
end
