class UpdateLocationBranches < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :update_location_lat, :string, after: :longitude
    add_column :branches, :update_location_lng, :string, after: :update_location_lat
    add_column :branches, :user_request_update, :integer, after: :update_location_lng
    add_column :branches, :status_request_update, :integer, default: 0, after: :user_request_update
  end
end
