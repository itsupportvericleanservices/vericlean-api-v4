class AddCompanyIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :client, after: :role_id, default: 0
  end
end
