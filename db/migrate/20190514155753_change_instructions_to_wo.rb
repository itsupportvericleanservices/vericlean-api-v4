class ChangeInstructionsToWo < ActiveRecord::Migration[5.2]
  def change
  	rename_column :work_orders, :instruction, :instructions
  end
end
