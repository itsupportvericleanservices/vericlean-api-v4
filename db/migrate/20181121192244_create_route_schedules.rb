class CreateRouteSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :route_schedules do |t|
      t.references :user
      t.references :route
      t.datetime :start_at
      t.datetime :finish_at
      t.datetime :started_at
      t.datetime :finished_at

      t.timestamps
    end
  end
end
