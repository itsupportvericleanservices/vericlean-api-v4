class AddStatusAltToWorkOrder < ActiveRecord::Migration[5.2]
  def change
  	add_column :work_orders, :status_alt, :integer, after: :status
  end
end
