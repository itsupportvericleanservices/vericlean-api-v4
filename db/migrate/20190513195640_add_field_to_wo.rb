class AddFieldToWo < ActiveRecord::Migration[5.2]
  def change
  	add_column :work_orders, :extra_id, :string, after: :internal_id
  end
end
