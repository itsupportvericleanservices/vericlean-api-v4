class AddReferenceToEmail < ActiveRecord::Migration[5.2]
  def change
  	add_reference :emails, :work_order, after: :user_id
  end
end
