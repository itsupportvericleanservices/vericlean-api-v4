class AddTextMessagesHashtag < ActiveRecord::Migration[5.2]
  def change
    add_column :text_messages, :hashtag, :string, after: 'message_type'
  end
end
