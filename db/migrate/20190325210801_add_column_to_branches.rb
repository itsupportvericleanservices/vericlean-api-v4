class AddColumnToBranches < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :deleted, :boolean, default: 0
  end
end
