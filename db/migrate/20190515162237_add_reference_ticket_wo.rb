class AddReferenceTicketWo < ActiveRecord::Migration[5.2]
  def change
  	add_reference :work_orders, :ticket, after: :client_id
  end
end
