class AddRouteToBranch < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :route, after: :frequency_id
  end
end
