class RenameReferenceTicket < ActiveRecord::Migration[5.2]
  def change
  	rename_column :tickets, :branche_id, :branch_id
  end
end
