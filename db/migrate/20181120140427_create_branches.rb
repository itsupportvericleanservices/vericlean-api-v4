class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.references :client, foreign_key: true
      t.string :name
      t.string :address
      t.string :address2
      t.string :state
      t.string :city
      t.string :zipcode
      t.string :phone
      t.string :email
      t.integer :min_time
      t.integer :max_time
      t.string :latitude
      t.string :longitude
      t.boolean :status

      t.timestamps
    end
  end
end
