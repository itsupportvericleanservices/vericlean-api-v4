class ChangeNameBranchesServiceTypes < ActiveRecord::Migration[5.2]
  def change
    rename_table :branches_servicetypes, :branches_service_types
  end
end
