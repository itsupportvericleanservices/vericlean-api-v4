class AddDelayedFlagToCheckins < ActiveRecord::Migration[5.2]
  def change
    add_column :route_histories, :delayed, :boolean, default: 0, after: :status
  end
end
