class DeleteAreaManagerSupervisorToBranchAddAreaManagerSupervisorToUser < ActiveRecord::Migration[5.2]
  def change
    change_table :branches do |t|
      t.remove :area_manager
      t.remove :supervisor
    end  
    add_column :users, :area_manager, :string
    add_column :users, :supervisor, :string
    add_column :users, :secundary_cleaner, :string
  end
end
