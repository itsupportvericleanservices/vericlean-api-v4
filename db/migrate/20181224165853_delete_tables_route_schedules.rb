class DeleteTablesRouteSchedules < ActiveRecord::Migration[5.2]
  def change
  	remove_reference :branches, :route_schedule, index: true
  	remove_reference :route_histories, :route_schedule, index: true
  	remove_reference :services, :route_schedule, index: true

  	drop_table :route_schedules
  end
end
