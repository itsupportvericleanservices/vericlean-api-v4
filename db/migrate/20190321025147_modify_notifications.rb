class ModifyNotifications < ActiveRecord::Migration[5.2]
  def change
    rename_column :notifications, :send_date, :sent_at
    rename_column :notifications, :device, :device_token
    add_column :notifications, :status, :boolean, after: :user_id
    add_column :notifications, :full_response, :text, after: :device_token
  end
end
