class CreateWorkOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :work_orders do |t|
    	t.string :internal_id
    	t.references :user
    	t.references :branch
    	t.references :cleaner
    	t.references :client
    	t.datetime :requested_date
    	t.datetime :due_date
    	t.string :site_contact
    	t.string :site_phone
    	t.string :request_contact
    	t.string :request_phone
    	t.text :task_description
    	t.integer :status
    	t.string :type
    	t.integer :priority
    	t.decimal :fee
    	t.datetime :scheduled_date
    	t.datetime :started_date
    	t.datetime :finished_date
    	t.string :started_lat
    	t.string :started_lng
    	t.string :finished_lat
    	t.string :finished_lng

      	t.timestamps

    end
  end
end
