class AddMutedActiveToBranch < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :active, :boolean
    add_column :branches, :muted, :boolean
  end
end