class AddMaxTimeSiteTypeSiteCodeToBranches < ActiveRecord::Migration[5.2]
  def change
    change_table :branches do |t|
      t.remove :max_time
      t.remove :min_time
    end  
    add_column :branches, :max_time, :time
    add_column :branches, :site_type, :string
    add_column :branches, :site_code, :string
  end
end
